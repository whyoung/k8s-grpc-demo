package why.demo.k8s.grpc.front.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import why.demo.k8s.grpc.front.config.GrpcClientConfig;
import why.demo.k8s.grpc.service.proto.IdRequest;

import java.util.HashMap;
import java.util.Map;

@RestController
public class FrontController {

    @Autowired
    private GrpcClientConfig clientConfig;

    @RequestMapping("/uuid")
    @ResponseBody
    public Map<String, String> getConfigs() {
        Map<String, String> resp = new HashMap<>();
        resp.put("uuid", clientConfig.getStub().getServerId(IdRequest.newBuilder().build()).getId());
        return resp;
    }
}
