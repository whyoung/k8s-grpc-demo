package why.demo.k8s.grpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import why.demo.k8s.grpc.service.proto.GrpcServiceGrpc;
import why.demo.k8s.grpc.service.proto.IdRequest;

public class Test {

    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("192.168.56.200", 18888).usePlaintext().build();
        GrpcServiceGrpc.GrpcServiceBlockingStub stub = GrpcServiceGrpc.newBlockingStub(channel);

        System.out.println("stub.getServerId(IdRequest.newBuilder().build()).getId() = " + stub.getServerId(IdRequest.newBuilder().build()).getId());

        channel.shutdown();
    }
}
