package why.demo.k8s.grpc.service.service;

import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import why.demo.k8s.grpc.backend.proto.LogRequest;
import why.demo.k8s.grpc.service.config.GrpcClientConfig;
import why.demo.k8s.grpc.service.proto.GrpcServiceGrpc;
import why.demo.k8s.grpc.service.proto.IdRequest;
import why.demo.k8s.grpc.service.proto.IdResponse;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Service
public class GrpcServiceImpl extends GrpcServiceGrpc.GrpcServiceImplBase {

    private String uuid;

    @Autowired
    private GrpcClientConfig config;

    @Override
    public void getServerId(IdRequest request, StreamObserver<IdResponse> responseObserver) {
        config.getStub().log(LogRequest.newBuilder().setHost(uuid).build());
        IdResponse response = IdResponse.newBuilder().setId(uuid).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @PostConstruct
    public void init() {
        this.uuid = UUID.randomUUID().toString();
    }
}
