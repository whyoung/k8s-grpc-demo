package why.demo.k8s.grpc.service.config;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import why.demo.k8s.grpc.backend.proto.BackGrpcServiceGrpc;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class GrpcClientConfig {

    @Value("${backend-host}")
    private String host;

    @Value("${backend-port}")
    private int port;

    private final LoadingCache<String, ManagedChannel> CHANNEL_CACHE =
            CacheBuilder.newBuilder()
                    .expireAfterAccess(1, TimeUnit.MINUTES)
                    .removalListener((RemovalListener<String, ManagedChannel>) l ->
                            l.getValue().shutdown())
                    .build(new CacheLoader<String, ManagedChannel>() {
                        @Override public ManagedChannel load(@Nonnull String address) {
                            return ManagedChannelBuilder.forTarget(address).usePlaintext().build();
                        }
                    });

    public BackGrpcServiceGrpc.BackGrpcServiceBlockingStub getStub() {
        ManagedChannel channel = CHANNEL_CACHE.getUnchecked(getProfileServiceAddress());
        if (channel.isShutdown() || channel.isTerminated()) {
            log.error("channel was shutdown({}) or terminated({})",
                    channel.isShutdown(), channel.isTerminated());
            CHANNEL_CACHE.refresh(getProfileServiceAddress());
        }
        return BackGrpcServiceGrpc.newBlockingStub(
                CHANNEL_CACHE.getUnchecked(getProfileServiceAddress()));
    }

    private String getProfileServiceAddress() {
        return host + ":" + port;
    }
}