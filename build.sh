#!/bin/sh

rm -f doc.zip
find doc/ -name '*.jar' | xargs rm -rf

mvn clean package

cp backend-impl/target/backend-impl-1.0-SNAPSHOT.jar doc/backend
cp service-impl/target/service-impl-1.0-SNAPSHOT.jar doc/service
cp front/target/front-1.0-SNAPSHOT.jar doc/front

zip doc.zip -r doc/

scp doc.zip root@172.16.227.4:/root/
scp doc.zip root@172.16.227.5:/root/

scp deploy.sh root@172.16.227.4:/root/
scp deploy.sh root@172.16.227.5:/root/

rm doc.zip