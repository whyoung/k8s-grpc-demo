package why.demo.k8s.grpc.backend.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import why.demo.k8s.grpc.backend.config.GrpcConfig;

@Component
public class GrpcCommandLineRunner implements CommandLineRunner {

    @Autowired
    GrpcConfig configuration;

    @Override
    public void run(String... args) throws Exception {
        configuration.start();
        configuration.block();
    }
}