package why.demo.k8s.grpc.backend.impl;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import why.demo.k8s.grpc.backend.proto.BackGrpcServiceGrpc;
import why.demo.k8s.grpc.backend.proto.LogRequest;
import why.demo.k8s.grpc.backend.proto.LogResponse;

@Slf4j
@Service
public class BackendGrpcServiceImpl extends BackGrpcServiceGrpc.BackGrpcServiceImplBase {

    @Override
    public void log(LogRequest request, StreamObserver<LogResponse> responseObserver) {
        log.warn("request from {}", request.getHost());
        responseObserver.onNext(LogResponse.newBuilder().build());
        responseObserver.onCompleted();
    }
}
