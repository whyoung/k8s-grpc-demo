#!/bin/sh

# clear doc dir，please check
rm -r doc/

unzip doc.zip

# build docker image
docker build -t ms/grpc-service:v1 doc/service/
docker build -t ms/grpc-front:v1 doc/front/
docker build -t ms/grpc-backend:v1 doc/backend/

# only execute on master node
if [[ $1 ]];then
  # create namespace dev
  if [[ `kubectl get ns dev | wc -l` -eq 0 ]];then
    kubectl create ns dev
  fi

  # apply
  kubectl apply -f doc/front/front.yaml -n dev
  kubectl apply -f doc/backend/backend.yaml -n dev
  kubectl apply -f doc/service/ConfigMap.yaml -n dev
  kubectl apply -f doc/service/service.yaml -n dev
fi

# test
# for i in {1..100}; do echo curl 127.0.0.1:`kubectl get svc -n dev grpc-front | awk 'NR>1{print substr($5,index($5, ":") + 1, 5)}'`/uuid;echo;done